package com.example.anas.myapplication4

import android.content.Intent
import android.os.Bundle
import android.provider.AlarmClock
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.facebook.login.LoginManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class MainActivity : AppCompatActivity() {
    var sginOutBtn :Button? = null
    var mAuth: FirebaseAuth? = null
    var user : FirebaseUser? = null
    var textView : TextView? = null
    val Pack = "com.example.anas.myapplication4"
    val Loginact = "com.example.anas.myapplication4.LoginActivity"
    val mainact = "com.example.anas.myapplication4.MainActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mAuth = FirebaseAuth.getInstance()
        user = mAuth!!.currentUser
        var useremail = user!!.displayName
        textView = findViewById(R.id.textView) as TextView
        textView!!.text = useremail
        sginOutBtn = findViewById(R.id.button) as Button
        sginOutBtn!!.setOnClickListener{
            mAuth!!.signOut()
            LoginManager.getInstance().logOut()
            if (mAuth!!.currentUser == null){
                Toast.makeText(this,"Signed out",Toast.LENGTH_SHORT).show()
            }
            gotoActivity(Loginact)
        }

    }
    fun gotoActivity(act: String) {
        var intent = Intent(act)
        intent.setClassName(Pack, act)
        intent.putExtra(AlarmClock.EXTRA_MESSAGE, "Logged in")
        startActivity(intent)

    }
}
